Overview
--------
This is an API for geo developers using PostGIS. PostGIS provides numerous functions (see http://postgis.net/docs/manual-2.0/reference.html) to handle, validate, convert and operate with geospatial geometries. This module provides the most widely used calls to this PostGIS functions which you can use from your module.

Examples
--------
An examples module is provided which calls the API functions through a simple UI. Thus you can learn how to use the API.

Requirements
------------
- PostgreSQL database
- PostGIS

Installation
------------
Install as usual. 
If you install the Geospatial Geometries Examples module (part of the package), go to Configuration / Geospatial Geometries Examples to get working

Useful tips
-----------
You can use Google Earth to produce sample data to work with. It allows you to create geo data in KLM, WKT and Geometry formats. With the Example UI you can start converting them into each other and to play around with them.

After installation some sample data are set by default. If you changed them, don't forget to save your data before you click the "Run example" button. Otherwise they will be reset on page load. If you ever mess up and want the default values restored, click the restore button. 